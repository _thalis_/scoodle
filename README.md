# Scoodle

Scoodle is a Doodle clone using [Lagom](https://www.lagomframework.com/) as backend microservice framework.
The base of Lagom and the [Play Framework](https://www.playframework.com/) used underneath is 
the programming language [Scala](https://www.scala-lang.org/). To stay in the eco-system of Scala and
to making reuse of domain-, helper- and utility-classes Scoodle's frontend is implemented with 
[ScalaJS](https://www.scala-js.org/) and the build tool to put it all together is the 
[Simple Build Tool (SBT)](http://www.scala-sbt.org/).

## Starting the local development environment

Lagom gives you a quite fast setup for developing and testing a microservice application. The
Lagom SBT Plugin offers a task ```runAll``` which starts a sophisticated hot-reloadable development
environment with the following services:

* a ServiceLocator as service registry (listening on port ```9008```)
* a Service Gateway as entry point into the microservice application (listening on port ```9000```)
* an [Apache Cassandra](http://cassandra.apache.org/) instance (port ```4000```) for persisting entities and their
  modification events (CQRS and ES)
* and an [Apache Kafka](https://kafka.apache.org/) (port ```9092```) message broker for decoupling the inter-service 
  communication
* whereas the Kafka uses a [Zookeeper](https://zookeeper.apache.org/) (listening on port ```2181```) which of course 
  is additionally started
* the application services getting a calculated port from 
  [IANA's _ephemeral port range_](https://en.wikipedia.org/wiki/Ephemeral_port) (ranging from ```49152``` to 
  ```65535```)

To start those services you have to install [SBT](https://www.scala-sbt.org/download.html) and enter ```sbt runAll```
within the project root. If one or any of the infrastructure services is locally available it's quite easy to
prevent the start of each of those services (e.g. by using ```lagomCassandraEnabled in ThisBuild := false```) and by
adding the local cassandra service to the ServiceLocator 
(```lagomUnmanagedServices in ThisBuild := Map("cas_native" -> "http://localhost:9042")```). See
section ["_Development Environment_"](https://www.lagomframework.com/documentation/current/scala/DevEnvironment.html)
for more information about adjusting the local development environment.

## Documentation of Scoodle

To read the documentation of Scoodle generate the site just enter ```sbt makeSite previewSite``` 
within the project root. A browser should open showing the Scoodle project page where the Scoodle documentation is 
linked.