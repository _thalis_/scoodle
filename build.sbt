organization in ThisBuild := "de.posteo.scheidewig.stefan"
version in ThisBuild := "1.0-SNAPSHOT"

scalaVersion in ThisBuild := "2.12.8"

lazy val ScoodleApi = (project in file("scoodle-api"))
  .settings(
    libraryDependencies ++= Seq(
      lagomScaladslApi,
      playJsonDerivedCodecs,
      playJson
    )
  )

lazy val scoodle = (project in file(".")).aggregate(ScoodleApi, ScoodleImpl, ScoodleDoc)

lazy val ScoodleImpl = (project in file("scoodle-impl"))
  .enablePlugins(LagomScala)
  .settings(
    libraryDependencies ++= Seq(
      lagomScaladslPersistenceCassandra,
      lagomScaladslKafkaBroker,
      lagomScaladslTestKit,
      macwire,
      playJson,
      scalaTest
    )
  )
  .settings(lagomForkedTestSettings: _*)
  .dependsOn(ScoodleApi)

lazy val ScoodleFrontend = (project in file("scoodle-frontend")).enablePlugins(ScalaJSPlugin, ScalaJSBundlerPlugin)
  .settings(
    npmDependencies in Compile ++= Seq(
      "@material-ui/core"  -> "3.1.0",
      "@material-ui/icons" -> "3.0.1",
      "react"              -> "16.4.2",
      "react-dom"          -> "16.4.2",
    ),
    libraryDependencies ++= Seq(
      "com.github.japgolly.scalacss"      %%% "core"         % "0.5.5",
      "com.github.japgolly.scalacss"      %%% "ext-react"    % "0.5.5",
      "com.github.japgolly.scalajs-react" %%% "core"         % "1.2.3",
      "com.github.japgolly.scalajs-react" %%% "extra"        % "1.2.3",
      "com.github.japgolly.scalajs-react" %%% "ext-monocle"  % "1.2.3",
      "org.scala-js"                      %%% "scalajs-dom"  % "0.9.6",
      "org.typelevel"                     %% "cats-core"     % "1.2.0",
      "com.github.julien-truffaut"        %% "monocle-core"  % "1.5.0",
      "com.github.julien-truffaut" %% "monocle-macro" % "1.5.0",
      playJson
    ),
    addCompilerPlugin("org.scalamacros" %% "paradise" % "2.1.1" cross CrossVersion.full),
    resolvers += Resolver.sonatypeRepo("public"),
    scalaJSUseMainModuleInitializer := true,
    scalaJSUseMainModuleInitializer in Test := false,
    webpackBundlingMode := BundlingMode.LibraryOnly(),
    version in webpack := "4.17.1",
    version in startWebpackDevServer := "3.1.7",
    webpackCliVersion := "3.1.0",
    emitSourceMaps in (Compile, fastOptJS) := true,
    emitSourceMaps in (Compile, fullOptJS) := false,
    relativeSourceMaps := true,
    webpackDevServerPort := 34567,
    webpackDevServerExtraArgs := Seq(
      "--history-api-fallback",
      "--content-base",
      (baseDirectory in ThisProject).value.getPath,
      "--open-page",
      "scoodle-dev.html"
    )
  )
  .dependsOn(ScoodleApi)

lazy val ScoodleDoc = (project in file("scoodle-doc")).enablePlugins(
    SiteScaladocPlugin,
    AsciidoctorPlugin,
    ParadoxSitePlugin
  )
  .settings(
    previewFixedPort := Some(12345),
    SiteScaladocPlugin.scaladocSettings(
      config("scoodle-api"),
      mappings in (Compile, packageDoc) in ScoodleApi,
      "api/scoodle-api"
    ),
    SiteScaladocPlugin.scaladocSettings(
      config("scoodle-impl"),
      mappings in (Compile, packageDoc) in ScoodleImpl,
      "api/scoodle-impl"
    ),
    sourceDirectory in Asciidoctor := sourceDirectory.value / "doc",
    siteSubdirName in Asciidoctor := "doc",
    sourceDirectory in Paradox := sourceDirectory.value / "site",
    siteSubdirName in Paradox := ""
  )

lazy val macwire = "com.softwaremill.macwire" %% "macros" % "2.3.2" % "provided"
lazy val scalaTest = "org.scalatest" %% "scalatest" % "3.0.5" % Test
lazy val playJsonDerivedCodecs = "org.julienrf" %% "play-json-derived-codecs" % "5.0.0"
lazy val playJson = "com.typesafe.play" %% "play-json" % "2.7.2"
