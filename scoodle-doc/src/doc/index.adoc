= Scoodle - A Hackathon 17/18 showcase application
Stefan Scheidewig <stefan.scheidewig@t-systems.com>
v1.0, 2018-08-28
:imagesdir: ./images
:iconsdir: ./icons
:stylesdir: ./css
:scriptsdir: ./js
:stylesheet: custom-boot-cosmo.css
:toc:

Scoodle aims to be a https://doodle.com[Doodle] clone implemented
with https://www.lagomframework.com[Lagom] microservice framework by
https://www.lightbend.com/[Lightbend] (formerly known as _Typesafe_) which
uses the https://www.playframework.com/[Play framework] to build
reactive applications. The frontend is built in https://www.scala-js.org/[Scala.js]
and to build the application the https://www.scala-sbt.org/[Simple Build Tool]
is used. As you can imagine the foundation of this technical stack
is the expressive object-oriented and functional programming language
https://www.scala-lang.org/[Scala] which runs on the JVM.

== The big picture

The idea of Scoodle is a showcase of what is possible with the Scala-stack. Scoodle
should provide a web application where people can create and participate in polls.
Users can create polls and for those polls links are generated referencing them.
One link is the administration link with which the user can alter the poll settings
and the other link is the poll link that can be sent to the participants of the poll.
The creator of the poll can add a list of email addresses that will be used to send a
generated poll invitation.

A participant calls the poll URL and can give one or more answers according to the poll
settings. The participant sees a visualization of the poll's current state if the poll results
are not secret. Those information will be updated without the need of refreshing the site. After the participant has
given his answers he has to press the answer button where he can optionally add an email address for being
notified about the poll results. An answer URL is shown to the participant (which is also
sent to him via mail if a mail address has been entered) where he can later adjust his answer. If
the url is lost the participant cannot alter his answer(s) afterwards.

== The detailed picture

If the user comes to the Scoodle main page he can create a new poll anonymously.
He presses the "create a new poll" button and comes to the configuration page of the new poll.

.The scoodle main page
image::main_page.svg[]

There he has to add a title and an optional description. He also has to choose the type of the poll
which can either be

 * a date poll with an option to specify times
 * a question/answer poll (simple - later complex ones will be added where the answers of previous question
   alter the residual poll)

whereas the polls can be configured by the following settings:

 * that the result is kept secret (can only be seen with a special result link)
 * the user should have a multiple choice
 * limit the number of participant of certain options
 * show the current poll results for an answer immediately after a choice has been made

The date and date/time polls can either be quickly configured with a time interval (for the date respectively
for the date and time) and weekdays. The user can also set the range and the precision of the time for each day
if selecting the date/time poll. For the question/answer poll the user has to add at least one question. The answer
can either be a "fixed option answer" where the answer options and a multiple choice option can be set or a "free
option answer" where the user has to answer with a short or long answer in a text field. The user can position
the Q/As and can globally decide if the next Q/A will be shown if the last one was answered.

After the poll settings are done a "send invitation" screen emerges where the poll creator can add one or more
mail address of people to be invited (mostly the address of a mailing list).

On the next screen the creator of the poll has to add the own email address and his name. After pressing the
"Finish" button a confirmation mail is sent to the creator's mail address. After pressing the confirmation link
the poll state switches from pending to created and the invitation mail(s) are being sent to the participants. The
creator receives a mail with the default poll url and an administration url where he can alter the poll settings.

The participants get invitation mails where the creator name, the title and description, general information
(time interval, number of questions, secret results, multiple choices, participant number limited) and first and
foremost the poll url with a jwt token where the mail address is woven and which have an expiration date. The
participant opens the pull by pressing the mail link. He makes his choices, can add an email address and sends
those poll results by pressing the complete button. The finish screen shows a participant url with which the
answers can be altered afterwards. If an email address has been added the participant gets a mail containing
the choices he made and this participant url.

After the poll has been completed by the participant a poll summary is shown and if the results are not secret a result
visualization is presented to the participant.