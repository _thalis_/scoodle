# Scoodle

Scoodle is a Doodle clone using [Lagom](https://www.lagomframework.com/) as backend microservice framework.
The base of Lagom and the [Play Framework](https://www.playframework.com/) used underneath is 
the programming language [Scala](https://www.scala-lang.org/). To stay in the eco-system of Scala and
to making reuse of domain-, helper- and utility-classes Scoodle's frontend is implemented with 
[ScalaJS](https://www.scala-js.org/) and the build tool to put it all together is the 
[Simple Build Tool (SBT)](http://www.scala-sbt.org/).

## Links

* [Documentation of Scoodle](/doc)
* [Scaladoc](/api)