package de.posteo.scheidewig.stefan.scoodle.impl

import akka.Done
import com.lightbend.lagom.scaladsl.persistence.PersistentEntity.ReplyType
import com.lightbend.lagom.scaladsl.persistence.{AggregateEvent, AggregateEventShards, AggregateEventTag, PersistentEntity}
import com.lightbend.lagom.scaladsl.playjson.{JsonSerializer, JsonSerializerRegistry}
import de.posteo.scheidewig.stefan.scoodle.api.ScoodlePoll
import play.api.libs.json._

import scala.collection.immutable.Seq

class PollEntity extends PersistentEntity {

  override type Command = ScoodleCommand[_]
  override type Event = PollEvent
  override type State = Option[ScoodlePoll]

  private val getPoll = Actions().onReadOnlyCommand[QueryPoll.type, Option[ScoodlePoll]] {
    case (QueryPoll, ctx, state) => ctx.reply(state)
  }

  private val created = {
    Actions().onCommand[ChangePoll, Done] {
      case (ChangePoll(changedPoll), ctx, _) =>
        ctx.thenPersist(PollChanged(changedPoll))(_ => ctx.reply(Done))
    }
      .onEvent {
        case (PollChanged(changedPoll), _) => Some(changedPoll)
      }
      .orElse(getPoll)
  }

  private val notCreated = {
    Actions().onCommand[CreatePoll, Done] {
      case (CreatePoll(newPoll), ctx, _) =>
        ctx.thenPersist(PollCreated(newPoll))(_ => ctx.reply(Done))
    }
      .onEvent {
        case (PollCreated(newPoll), _) => Some(newPoll)
      }
      .orElse(getPoll)
  }

  /*
   * Returns the name of the entity type. To be refactoring-safe it should
   * be a fixed value (the default is the fq name of the entity class so it
   * would become complicated if the class' package would ever change).
   */
  override def entityTypeName: String = "Poll"

  override def initialState: Option[ScoodlePoll] = None

  override def behavior: Behavior = {

    case None    => notCreated
    case Some(_) => created
  }
}

/*
 * =Events=
 */

sealed trait PollEvent extends AggregateEvent[PollEvent] {
  def aggregateTag: AggregateEventShards[PollEvent] = PollEvent.Tag
}

object PollEvent {
  val NumShards = 2

  val Tag: AggregateEventShards[PollEvent] =
    AggregateEventTag.sharded[PollEvent](NumShards)
}

case class PollChanged(changedPoll: ScoodlePoll) extends PollEvent

object PollChanged {
  implicit val format: Format[PollChanged] = Json.format
}

case class PollCreated(newPoll: ScoodlePoll) extends PollEvent

object PollCreated {
  implicit val format: Format[PollCreated] = Json.format
}

/*
 * =Commands=
 */

sealed trait ScoodleCommand[R] extends ReplyType[R]

case class ChangePoll(changedPoll: ScoodlePoll) extends ScoodleCommand[Done]

object ChangePoll {
  implicit val format: Format[ChangePoll] = Json.format
}

case class CreatePoll(newPoll: ScoodlePoll) extends ScoodleCommand[Done]

object CreatePoll {
  implicit val format: Format[CreatePoll] = Json.format
}

case object QueryPoll extends ScoodleCommand[Option[ScoodlePoll]] {
  implicit val format: Format[QueryPoll.type] = Format[QueryPoll.type](
    {
      case JsString("QueryPoll") => JsSuccess(QueryPoll)
      case _                     => JsError("Cannot parse to QueryPoll")
    },
    (o: QueryPoll.type) => JsString(QueryPoll.toString)
  )
}

/**
  * Akka serialization, used by both persistence and remoting, needs to have
  * serializers registered for every type serialized or deserialized. While it's
  * possible to use any serializer you want for Akka messages, out of the box
  * Lagom provides support for JSON, via this registry abstraction.
  *
  * The serializers are registered here, and then provided to Lagom in the
  * application loader. It should be an extra object definition to make it
  * reusable in integration tests.
  */
object ScoodleSerializerRegistry extends JsonSerializerRegistry {
  override def serializers: Seq[JsonSerializer[_]] = Seq(
    JsonSerializer[ChangePoll],
    JsonSerializer[PollChanged],
    JsonSerializer[CreatePoll],
    JsonSerializer[PollCreated],
    JsonSerializer[QueryPoll.type],
    JsonSerializer[ScoodlePoll]
  )
}
