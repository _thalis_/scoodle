package de.posteo.scheidewig.stefan.scoodle.impl

import java.util.UUID

import akka.persistence.query.Offset
import com.datastax.driver.core.utils.UUIDs
import com.lightbend.lagom.scaladsl.api.ServiceCall
import com.lightbend.lagom.scaladsl.api.broker.Topic
import com.lightbend.lagom.scaladsl.api.transport.NotFound
import com.lightbend.lagom.scaladsl.broker.TopicProducer
import com.lightbend.lagom.scaladsl.persistence.{EventStreamElement, PersistentEntityRegistry}
import de.posteo.scheidewig.stefan.scoodle.api
import de.posteo.scheidewig.stefan.scoodle.api.{PollService, ScoodlePoll}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

/**
  * Implementation of the ScoodleService.
  */
class PollServiceImpl(persistentEntityRegistry: PersistentEntityRegistry) extends PollService {

  persistentEntityRegistry.register(new PollEntity)

  /**
    * Example: curl http://localhost:9000/api/poll/1234-4366-3342-4432-4263
    */
  override def getPoll(id: UUID) = ServiceCall { _ =>
    val ref = persistentEntityRegistry.refFor[PollEntity](id.toString)

    ref.ask(QueryPoll).map {
      case Some(poll) => toApiPoll(poll)
      case None       => throw NotFound(s"Poll with id $id not found")
    }
  }

  private def toApiPoll(poll: ScoodlePoll): api.ScoodlePoll =
    api.ScoodlePoll(poll.id, poll.title, poll.description, poll.location)

  /**
    * Example: curl -H "Content-Type: application/json" -X POST -d '{"title":
    * "A poll", "description": "A poll description"}' http://localhost:9000/api/poll
    */
  override def createPoll = ServiceCall { request =>
    val newPollId = UUIDs.timeBased()

    val ref = persistentEntityRegistry.refFor[PollEntity](newPollId.toString)

    val newPoll = ScoodlePoll(newPollId, request.title, request.description, request.location)

    ref.ask(CreatePoll(newPoll)).map { _ => newPoll
    }
  }

  override def savePoll(id: UUID) = ServiceCall { request =>
    val ref         = persistentEntityRegistry.refFor[PollEntity](id.toString)
    val changedPoll = ScoodlePoll(request.id, request.title, request.description, request.location)
    ref.ask(ChangePoll(changedPoll)).map { _ => changedPoll
    }
  }

  override def pollsTopic: Topic[api.PollEvent] =
    TopicProducer.taggedStreamWithOffset(PollEvent.Tag.allTags.toList) { (tag, fromOffset) =>
      persistentEntityRegistry
        .eventStream(tag, fromOffset)
        .filter {
          _.event match {
            case x @ (_: PollCreated | _: PollChanged) => true
            case _                                     => false
          }
        }
        .mapAsync(1)(convertEvent)
    }

  private def convertEvent(eventStreamElement: EventStreamElement[PollEvent]): Future[(api.PollEvent, Offset)] =
    eventStreamElement match {
      case EventStreamElement(_, PollCreated(newPoll), offset) =>
        Future.successful {
          (api.PollCreated(newPoll), offset)
        }
      case EventStreamElement(_, PollChanged(changedPoll), offset) =>
        Future.successful {
          (api.PollChanged(changedPoll), offset)
        }
    }
}
