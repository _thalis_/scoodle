package de.posteo.scheidewig.stefan.scoodle.impl

import com.lightbend.lagom.scaladsl.api.ServiceLocator
import com.lightbend.lagom.scaladsl.api.ServiceLocator.NoServiceLocator
import com.lightbend.lagom.scaladsl.broker.kafka.LagomKafkaComponents
import com.lightbend.lagom.scaladsl.devmode.LagomDevModeComponents
import com.lightbend.lagom.scaladsl.persistence.cassandra.CassandraPersistenceComponents
import com.lightbend.lagom.scaladsl.server._
import com.softwaremill.macwire._
import de.posteo.scheidewig.stefan.scoodle.api.PollService
import play.api.libs.ws.ahc.AhcWSComponents

/**
  * Boots/loads the service by defining an [[com.lightbend.lagom.scaladsl.server.LagomApplication]]
  * implementation (see
  * [[de.posteo.scheidewig.stefan.scoodle.impl.ScoodleApplication the "''cake pattern''" explanation]]).
  */
class ScoodleLoader extends LagomApplicationLoader {

  override def load(context: LagomApplicationContext): LagomApplication =
    new ScoodleApplication(context) {
      override def serviceLocator: ServiceLocator = NoServiceLocator
    }

  override def loadDevMode(context: LagomApplicationContext): LagomApplication =
    new ScoodleApplication(context) with LagomDevModeComponents

  override def describeService = Some(readDescriptor[PollService])
}

/**
  * An example of the so-called "''cake pattern''" - a dependency injection
  * mechanism where several parts are abstract and at the starting time
  * the missing methods are overridden by extending components supplying
  * implementations of those missing methods or giving implicit context
  * objects. See the `loadDevMode` method where the abstract
  * [[de.posteo.scheidewig.stefan.scoodle.impl.ScoodleApplication]]
  * is completed with a local service locator - compared
  * to the `load` method where the `serviceLocator` is set to not available
  * for a production mode.
  *
  * In this very case we are baking in the Cassandra persistence, the Kafka
  * broker as well as the AsyncHttpClient client implementation for calling
  * other services synchronously.
  */
abstract class ScoodleApplication(context: LagomApplicationContext)
    extends LagomApplication(context)
    with CassandraPersistenceComponents
    with LagomKafkaComponents
    with AhcWSComponents {

  /**
    * Binds the service that this server provides. Uses Macwire to look up and bind the
    * [[de.posteo.scheidewig.stefan.scoodle.impl.PollServiceImpl PollServiceImpl]] implementation
    * to the PollService server.
    */
  override lazy val lagomServer: LagomServer =
    serverFor[PollService](wire[PollServiceImpl])

  // Register the JSON serializer registry
  override lazy val jsonSerializerRegistry: ScoodleSerializerRegistry.type =
    ScoodleSerializerRegistry
}
