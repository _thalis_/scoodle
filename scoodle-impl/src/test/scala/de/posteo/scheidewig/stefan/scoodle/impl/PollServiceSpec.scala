package de.posteo.scheidewig.stefan.scoodle.impl

import com.lightbend.lagom.scaladsl.server.LocalServiceLocator
import com.lightbend.lagom.scaladsl.testkit.ServiceTest
import de.posteo.scheidewig.stefan.scoodle.api._
import org.scalatest.{AsyncWordSpec, BeforeAndAfterAll, Matchers}

class PollServiceSpec extends AsyncWordSpec with Matchers with BeforeAndAfterAll {

  lazy val client: PollService = server.serviceClient.implement[PollService]

  private lazy val server = ServiceTest.startServer(ServiceTest.defaultSetup.withCassandra()) { ctx =>
    new ScoodleApplication(ctx) with LocalServiceLocator
  }

  override protected def beforeAll(): Unit = server

  override protected def afterAll(): Unit = server.stop()

  "scoodle service" should {

    "give back a poll for a created existing poll" in {
      for {
        poll        <- client.createPoll.invoke(NewPoll("Another poll"))
        fetchedPoll <- client.getPoll(poll.id).invoke()
      } yield {
        fetchedPoll.title should be("Another poll")
      }
    }

    "change an existing poll" in {
      for {
        poll        <- client.createPoll.invoke(NewPoll("A niuew poll"))
        changedPoll <- client.savePoll(poll.id).invoke(ScoodlePoll(poll.id, "A new poll", Some("Awesome location")))
        fetchedPoll <- client.getPoll(changedPoll.id).invoke()
      } yield {
        fetchedPoll.title should be("A new poll")
      }
    }
  }
}
