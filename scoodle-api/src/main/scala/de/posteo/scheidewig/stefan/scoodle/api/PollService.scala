package de.posteo.scheidewig.stefan.scoodle.api

import java.util.UUID

import akka.NotUsed
import com.lightbend.lagom.scaladsl.api.broker.Topic
import com.lightbend.lagom.scaladsl.api.transport.Method
import com.lightbend.lagom.scaladsl.api.{Descriptor, Service, ServiceCall}

object PollService {
  val TOPIC_NAME = "scoodle-PollEvents"
}

trait PollService extends Service {

  final override def descriptor: Descriptor = {
    import Service._
    // Services get a name for registration within the service locator
    named("poll")
      .withCalls(
        pathCall("/api/poll/:id", getPoll _),
        restCall(Method.POST, "/api/poll/:id", savePoll _),
        pathCall("/api/poll", createPoll)
      )
      .withTopics(
        topic(PollService.TOPIC_NAME, pollsTopic)
      )
      .withAutoAcl(true)
  }

  /**
    * Example: curl http://localhost:9000/api/poll/1234-4366-3342-4432-4263
    */
  def getPoll(id: UUID): ServiceCall[NotUsed, ScoodlePoll]

  /**
    * Example: curl -H "Content-Type: application/json" -X POST -d '{"title":
    * "A poll", "description": "A poll description"}' http://localhost:9000/api/poll
    */
  def createPoll: ServiceCall[NewPoll, ScoodlePoll]

  def savePoll(id: UUID): ServiceCall[ScoodlePoll, ScoodlePoll]

  /**
    * This gets published to Kafka.
    */
  def pollsTopic: Topic[PollEvent]
}
