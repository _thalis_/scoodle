package de.posteo.scheidewig.stefan.scoodle.api

import julienrf.json.derived
import play.api.libs.json._

sealed trait PollEvent {
  val poll: ScoodlePoll
}

final case class PollCreated(poll: ScoodlePoll) extends PollEvent

object PollCreated {
  implicit val format: Format[PollCreated] = Json.format
}

final case class PollChanged(poll: ScoodlePoll) extends PollEvent

object PollChanged {
  implicit val format: Format[PollChanged] = Json.format
}

object PollEvent {
  implicit val format: Format[PollEvent] =
    derived.flat.oformat((__ \ "type").format[String])
}
