package de.posteo.scheidewig.stefan.scoodle.api

import java.util.UUID

import play.api.libs.json.{Format, Json}

sealed trait Poll {
  def title: String

  def description: Option[String] = None

  def location: Option[String] = None
}

final case class NewPoll(title: String,
                         override val description: Option[String] = None,
                         override val location: Option[String] = None)
    extends Poll

object NewPoll {
  implicit val format: Format[NewPoll] = Json.format[NewPoll]
}

final case class ScoodlePoll(id: UUID,
                             title: String,
                             override val description: Option[String] = None,
                             override val location: Option[String] = None)
    extends Poll

object ScoodlePoll {
  implicit val format: Format[ScoodlePoll] = Json.format[ScoodlePoll]
}
