package de.posteo.scheidewig.scoodle.frontend.components

import com.pangwarta.sjrmui.{AppBar, Toolbar, Typography}
import japgolly.scalajs.react.ScalaComponent
import japgolly.scalajs.react.vdom.html_<^._

object ScoodleAppBar {

  private lazy val component = ScalaComponent
    .builder[Unit]("ScoodleAppBar")
    .renderStatic(
      AppBar(
        position = AppBar.static,
        color = AppBar.default
      )()(
        Toolbar()()(
          Typography(
            variant = Typography.Variant.title,
            color = Typography.Color.inherit
          )()("Scoodle")
        )
      )
    )
    .build

  def apply() = component()

}
