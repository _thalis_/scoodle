package de.posteo.scheidewig.scoodle.frontend.pages

import com.pangwarta.sjrmui.{Step, Stepper}
import de.posteo.scheidewig.scoodle.frontend.components.PollCreationStep.Props
import de.posteo.scheidewig.scoodle.frontend.components.{PollCreationStep, TitleDescriptionDialog}
import de.posteo.scheidewig.scoodle.frontend.model.NewPollState
import japgolly.scalajs.react.MonocleReact._
import japgolly.scalajs.react._
import japgolly.scalajs.react.extra.StateSnapshot
import japgolly.scalajs.react.vdom.html_<^._

object NewPollPage {

  private lazy val component =
    ScalaComponent
      .builder[Unit]("NewPollPage")
      .initialState(NewPollState())
      .renderS { ($, state) =>
        Stepper(activeStep = 0, orientation = Step.Orientation.vertical.get)()(
          PollCreationStep(Props("Titel und Beschreibung", 0))(
            TitleDescriptionDialog(StateSnapshot.zoomL(NewPollState.descriptionAndTitle).of($))
          ),
          PollCreationStep(Props("Fragen", 1))(
            EmptyVdom
          ),
          PollCreationStep(Props("Einstellungen", 2))(
            EmptyVdom
          ),
          PollCreationStep(Props("Name und Kontakt", 3))(
            EmptyVdom
          )
        )
      }
      .build

  def apply() = component()

}
