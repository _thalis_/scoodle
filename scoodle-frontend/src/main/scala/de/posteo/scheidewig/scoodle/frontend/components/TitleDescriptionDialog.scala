package de.posteo.scheidewig.scoodle.frontend.components

import com.pangwarta.sjrmui.{Grid, TextField}
import de.posteo.scheidewig.scoodle.frontend.model.DescriptionAndTitle
import japgolly.scalajs.react._
import japgolly.scalajs.react.extra.StateSnapshot
import japgolly.scalajs.react.vdom.html_<^._

object TitleDescriptionDialog {

  def apply(stateSnapshot: StateSnapshot[DescriptionAndTitle]): VdomNode =
    ScalaFnComponent[StateSnapshot[DescriptionAndTitle]] { stateSnapshot =>
      Grid(container = true, direction = Grid.Direction.column, xs = 12, sm = 12, md = 9, lg = 6)()(
        TextField(
          id = "pollTitle",
          label = "Titel",
          margin = TextField.normal.get,
        )("variant" -> "outlined")(),
        TextField(
          id = "pollDescription",
          label = "Beschreibung",
          margin = TextField.normal.get,
          multiline = true,
          rows = 5,
          rowsMax = 15
        )("variant" -> "outlined")()
      )
    }(stateSnapshot)
}
