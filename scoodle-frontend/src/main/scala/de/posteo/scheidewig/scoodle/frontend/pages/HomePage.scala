package de.posteo.scheidewig.scoodle.frontend.pages

import com.pangwarta.sjrmui.Button
import de.posteo.scheidewig.scoodle.frontend.routes.AppRouter.{AppPage, NewPoll}
import japgolly.scalajs.react._
import japgolly.scalajs.react.extra.router.RouterCtl
import japgolly.scalajs.react.vdom.html_<^._
import scalacss.ProdDefaults._
import scalacss.ScalaCssReact._

object HomePage {

  val component = ScalaComponent
    .builder[Props]("HomePage")
    .render_P { props => <.div(Style.content, Button(onClick = props.routerControl.setEH(NewPoll))()("Neue Umfrage"))
    }
    .build

  def apply(props: Props) = component(props)

  case class Props(routerControl: RouterCtl[AppPage])

  private object Style extends StyleSheet.Inline {
    import dsl._
    val content = style(textAlign.center, fontSize(30.px), minHeight(450.px), paddingTop(40.px))
  }

}
