package de.posteo.scheidewig.scoodle.frontend.model

import monocle.macros._

sealed trait PollBuilderStep {
  def isComplete: Boolean
}

@Lenses
final case class DescriptionAndTitle(title: Option[String] = None, description: Option[String] = None)
    extends PollBuilderStep {
  override def isComplete: Boolean = title.isDefined
}

@Lenses
final case class Question[QuestionType](item: QuestionType)

@Lenses
final case class Questions[QuestionType](items: Seq[QuestionType] = Seq()) extends PollBuilderStep {
  override def isComplete: Boolean = items.size > 2
}

@Lenses
final case class Settings(isSecret: Boolean = false) extends PollBuilderStep {
  override def isComplete: Boolean = true
}

@Lenses
final case class CreatorContact(creatorName: Option[String] = None, mail: Option[String] = None)
    extends PollBuilderStep {
  override def isComplete: Boolean =
    creatorName.isDefined && creatorName.get.length > 2 &&
      mail.isDefined && mail.get.length > 2 // actually also checking for valid mail address
}

@Lenses
final case class NewPollState[QuestionType](
  descriptionAndTitle: DescriptionAndTitle = DescriptionAndTitle(),
  questions: Questions[QuestionType] = Questions[QuestionType](),
  settings: Settings = Settings(),
  contact: CreatorContact = CreatorContact()
)
