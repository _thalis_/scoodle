package de.posteo.scheidewig.scoodle.frontend.components

import com.pangwarta.sjrmui.{Step, StepContent, StepLabel}
import japgolly.scalajs.react._
import japgolly.scalajs.react.vdom.html_<^._

object PollCreationStep {

  def apply(props: Props)(children: VdomNode*): VdomNode =
    ScalaFnComponent.withChildren[Props] { (props, children) =>
      import props._
      Step(index = stepNumber, active = stepNumber == 0)()(
        StepLabel()()(stepName),
        StepContent()()(children)
      )
    }(props)(children: _*)

  case class Props(stepName: String, stepNumber: Int)

}
