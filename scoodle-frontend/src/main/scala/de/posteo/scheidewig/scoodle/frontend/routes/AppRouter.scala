package de.posteo.scheidewig.scoodle.frontend.routes

import de.posteo.scheidewig.scoodle.frontend.components.ScoodleAppBar
import de.posteo.scheidewig.scoodle.frontend.pages.HomePage.Props
import de.posteo.scheidewig.scoodle.frontend.pages.{HomePage, NewPollPage}
import japgolly.scalajs.react.extra.router.{Resolution, RouterConfigDsl, RouterCtl, _}
import japgolly.scalajs.react.vdom.html_<^._

object AppRouter {

  val routerConfig: RouterConfig[AppPage] = RouterConfigDsl[AppPage].buildConfig { dsl =>
    import dsl._
    (trimSlashes
      | staticRoute(root, Home) ~> renderR(routerCtl => HomePage(Props(routerCtl)))
      | staticRoute("#newpoll", NewPoll) ~> render(NewPollPage()))
      .notFound(redirectToPage(Home)(Redirect.Replace))
      .renderWith(layout)
  }
  val router = Router(BaseUrl.fromWindowOrigin / "scoodle", routerConfig)

  def layout(routerControl: RouterCtl[AppPage], resolution: Resolution[AppPage]) =
    <.div(
      ScoodleAppBar(),
      resolution.render()
    )

  sealed trait AppPage

  final case object Home extends AppPage

  final case object NewPoll extends AppPage
}
