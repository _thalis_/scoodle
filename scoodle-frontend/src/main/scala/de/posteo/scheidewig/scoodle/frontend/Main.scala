package de.posteo.scheidewig.scoodle.frontend

import de.posteo.scheidewig.scoodle.frontend.routes.AppRouter
import org.scalajs.dom

object Main {

  def main(args: Array[String]): Unit =
    AppRouter.router().renderIntoDOM(dom.document.getElementById("root"))

}
